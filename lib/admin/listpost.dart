import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ListPostPage extends StatefulWidget{
  _ListPostPageState createState()=> _ListPostPageState();
}

class _ListPostPageState extends State<ListPostPage>{
  List posts = [];
  Future getAllPost()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/postAll.php");
    var response = await http.get(url);
    if(response.statusCode == 200)
      {
        var jsonData = json.decode(response.body);
        setState(() {
          posts = jsonData;
        });
      }
    print(posts);
  }
  @override
  void initState()
  {
    super.initState();
    getAllPost();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("Danh Sách bài viết"),),
      body:   ListView.builder(
          itemCount: posts.length,
          itemBuilder: (context, index) {
       return Card(
         elevation: 2,
         child: ListTile(
           leading: IconButton(
             icon: Icon(Icons.edit),
             onPressed: (){},
           ),
           title: Text(posts[index]['name']),
           subtitle:Text(posts[index]['content'], maxLines: 2,),
           trailing: IconButton(
             icon: Icon(Icons.delete),
             onPressed: (){},
           ),
         ),
       );
      }),
    );
  }
  
}