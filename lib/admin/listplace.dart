import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:untitled1/admin/addeditplace.dart';

class ListPlacePage extends StatefulWidget{
  _ListPlacePageState createState()=> _ListPlacePageState();
}

class _ListPlacePageState extends State<ListPlacePage>{
  List places = [];
  Future getAllPlace()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/placeAll.php");
    var response = await http.get(url);
    if(response.statusCode == 200)
    {
      var jsonData = json.decode(response.body);
      setState(() {
        places = jsonData;
      });
    }
    print(places);
  }
  @override
  void initState()
  {
    super.initState();
    getAllPlace();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(title: Text("Danh Sách bài viết"),
      actions: <Widget>[
        IconButton(onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=> AddEditPlace(),),);
        }, icon: Icon(Icons.add))
      ],),
      body:   ListView.builder(
          itemCount: places.length,
          itemBuilder: (context, index) {
            return Card(
              elevation: 2,
              child: ListTile(
                leading: IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> AddEditPlace(placelist: places,index: index,),),);
                  },
                ),
                title: Text(places[index]['tendiadiem']),
                subtitle:Text(places[index]['diachi'], maxLines: 2,),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: (){

                  },
                ),
              ),
            );
          }),
    );
  }

}