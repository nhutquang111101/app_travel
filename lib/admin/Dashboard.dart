import 'package:flutter/material.dart';
import 'package:untitled1/admin/listplace.dart';

import '../listcity.dart';
import 'listpost.dart';

class DashboardPage extends StatefulWidget{
  final name;
  final email;
  final id;
  DashboardPage({this.name = "", this.email = "", this.id});
  @override
  _DashboardPageState createState()=> _DashboardPageState();
}
class _DashboardPageState extends State<DashboardPage>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        elevation: 1,
        title: Container(
          margin: EdgeInsets.only(left: 90),
          child: Text("Dashboard", style: TextStyle(fontWeight: FontWeight.bold),),
        ),
      ),
      drawer: Drawer(child:  ListView(children:<Widget> [
        ListTile(
          onTap: (){
             Navigator.push(context, MaterialPageRoute(builder: (context)=> listcity()));

          },
          leading: Icon(Icons.location_city, color: Colors.blueAccent,),
          title: Text("Danh sách Tỉnh Thành", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blueAccent),),
        ),

        ListTile(
          onTap: (){
             Navigator.push(context, MaterialPageRoute(builder: (context)=> ListPostPage(),),);
          },
          leading: Icon(Icons.newspaper, color: Colors.green,),
          title: Text("Danh Sách bài viết", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green),),
        ) ,
        ListTile(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> ListPlacePage(),),);
          },
          leading: Icon(Icons.place, color: Colors.deepPurpleAccent,),
          title: Text("Danh Sách địa điểm", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurpleAccent),),
        ) ,
        ListTile(
          onTap: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=> MyHomePage(),),);
          },
          leading: Icon(Icons.logout, color: Colors.redAccent,),
          title: Text("Đăng Xuất", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.redAccent),),
        ),
      ],),),
      body: Container(
        child: Column(
          children: [
            Text("Xin Chào "+ widget.name),
            Text('Username: '+ widget.email),
            Text("" + widget.id),
          ],
        ),
      ),
    );
  }

}