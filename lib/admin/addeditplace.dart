
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddEditPlace extends StatefulWidget{
  final placelist;
  final index;
  AddEditPlace({this.placelist, this.index});

  _AddEditPlaceState createState()=>_AddEditPlaceState();
}
class _AddEditPlaceState extends State<AddEditPlace>{

    File? _image;
    final picker = ImagePicker();
  TextEditingController nameplace = TextEditingController();
  TextEditingController addressplace = TextEditingController();

  bool editMode = false;
  Future choiceImage()async{
      // ignore: deprecated_member_use
      var pickerimage = await picker.getImage(source: ImageSource.gallery);
      setState(() {
        _image = File(pickerimage!.path);
      });
  }

  @override
  void initState()
  {
    super.initState();
    if(widget.index != null)
      {
        editMode = true;
        nameplace.text = widget.placelist[widget.index]['tendiadiem'];
        addressplace.text = widget.placelist[widget.index]['diachi'];
      }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(editMode ? 'Update' : 'Add Place'),
      ),
      body: ListView(children: <Widget>[
        Padding(padding: EdgeInsets.all(8.0),
        child: TextField(
          controller: nameplace,
          decoration: InputDecoration(labelText: 'Tên Địa Điểm'),),
        ),
        Padding(padding: EdgeInsets.all(8.0),
          child: TextField(
            controller: addressplace,
            decoration: InputDecoration(labelText: 'Địa chỉ'),),
        ),
        IconButton(onPressed: (){
          choiceImage();
        }, icon: Icon(Icons.picture_in_picture, size: 60,),),
        Container(
          child: _image == null ? Text("Khong duoc de trong hinh anh") : Image.file(_image!), width: 100,height: 100,
        ),
      ],),

    );
  }

}