import 'package:flutter/material.dart';

import 'Button.dart';
import 'inputfield.dart';

class InputWrapper extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Padding(padding: EdgeInsets.all(30),
        child: Column(
          children:<Widget> [
            SizedBox(height: 40),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: InputField(),
            ),
            SizedBox(height: 40),
            Text('Quên Mật Khẩu?', style: TextStyle(color: Colors.grey),
            ),
            SizedBox(height: 40),
            Button(),
          ],
        ),
    );
  }
}