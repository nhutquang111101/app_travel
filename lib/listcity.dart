import 'dart:convert';
import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'addplace.dart';


 class listcity extends StatefulWidget{
   @override
   _listcityState createState() => _listcityState();

 }
 class _listcityState extends State<listcity>{
   Future getCity() async {
     try {
       var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/listcity.php");
       final response = await http.get(url);
       return json.decode(response.body);
     } catch (excute) {
       print("Error: $excute");
       return "Errror";
     }
   }
  @override
  Widget build(BuildContext context) {
      return Scaffold(
      appBar: AppBar(
        title: Text("Danh Sách Thành Phố",
          style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold),
        ),
      ),
      body: FutureBuilder(
        future: getCity(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasError) print("Error");
          return snapshot.hasData ? ListView.builder(
            itemCount: snapshot.data.length == null ? 0 : snapshot.data.length,
            itemBuilder: (context, index) {
              List list  = snapshot.data;
              return ListTile(
                leading: GestureDetector(
                  onTap: (){
                    // Navigator.push(context, MaterialPageRoute(builder: (context)=>AddPage(list: list, index: index)));
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>UpdatePage(list: list, index: index)));
                    debugPrint("Edit button clicked");
                  },
                  child: Icon(Icons.edit),
                ),
                title: Text(list[index]['name']),
                trailing: GestureDetector(
                  onTap: (){
                    setState(() {
                      var urldelete = Uri.parse("http://192.168.1.180/Flutter_Apptravel/delete.php");
                      http.post(urldelete, body: {
                        'id' : list[index]['id'],
                      });
                    });
                    debugPrint("Edit button clicked");
                  },
                  child: Icon(Icons.delete),
                ) ,
              );
            },
          ) : Center(child: CircularProgressIndicator(),);
        },
      ),
    );
  }
 }

