import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'Page/SelectCity.dart';

class CityListItem extends StatefulWidget{
  @override
  _CityListItemState createState() => _CityListItemState();
}

class _CityListItemState extends State<CityListItem>{
  List citylist = [];

  Future getAllCity() async {
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/listcity.php");
    var response = await http.get(url);
      var jsonData = json.decode(response.body);
      setState(() {
        citylist = jsonData;
      });

    // return json.decode(response.body);
  }

  @override
  void initState()
  {
    super.initState();
    getAllCity();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 70,
      child: ListView.builder(
      scrollDirection: Axis.horizontal,
        itemCount: citylist.length,
        itemBuilder: (context, index) {
        return CityItem(name: citylist[index]['name'], id: citylist[index]['id'],);
      }),
    );
  }
}

class CityItem extends StatefulWidget{
  final id;
  final name;
  CityItem({Key? key, this.name, this.id}) : super(key: key);
  _CityItemState createState() => _CityItemState();
}

class _CityItemState extends State<CityItem>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: InkWell(child: Text(widget.name),
          onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => SelectCityBy(idcity: widget.id,namecityOfplace: widget.name,),),);
          debugPrint(widget.name);
          },
        ),
      ),
    );
  }
}