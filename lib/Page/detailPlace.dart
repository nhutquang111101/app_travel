import 'package:flutter/material.dart';

import 'listPostByPlace.dart';

class PlaceDetail extends StatelessWidget{
  final iddiadiem;
  final tendiadiem;
  final diachi;
  final imgdiadiem;
  final userid;
  final idtinhthanh;
  PlaceDetail({Key? key,this.iddiadiem, this.tendiadiem, this.diachi, this.imgdiadiem, this.userid, this.idtinhthanh }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(left: 50),
          child: Text("Chi Tiết bài Viết", style: TextStyle(fontWeight: FontWeight.bold),),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(8.0),
              child: Text(tendiadiem, style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),),
            ),
            SizedBox(height: 20,),
            Container( child: Image.network(imgdiadiem),),
            SizedBox(height: 20,),
            Padding(padding: EdgeInsets.all(8.0),
              child: Text("Địa Điểm: "+diachi, style: TextStyle(
                fontSize: 18,
              ),),
            ),
            SizedBox(height: 20,),
            Padding(padding: EdgeInsets.all(8.0),
              child: Text("Tạo Bởi: "+ userid, style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),),
            ),
            Container(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  alignment: Alignment.center,
                  primary: Colors.lightBlue,
                  elevation: 5,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50),),
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 130),
                ),
                onPressed: (){
                   Navigator.push(context, MaterialPageRoute(builder: (context)=>listItemPost(idplace: iddiadiem, nameplace: tendiadiem,) ));

                },
                child: Text('Xem Ngay',
                  style: TextStyle(color: Colors.white,fontSize: 18),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}