import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:untitled1/Page/loginpage.dart';

import '../admin/Dashboard.dart';
import '../main.dart';

class SignUppage extends StatefulWidget{
  const  SignUppage({Key? key}) : super (key: key);
  @override
  _SignUppageState createState() => _SignUppageState();
}
class _SignUppageState extends State<SignUppage>{
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController passwordconfirm = new TextEditingController();
  TextEditingController fullname = new TextEditingController();
  TextEditingController gender = new TextEditingController();
  // TextEditingController roles = new TextEditingController();

  Future signup() async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/login.php");
    var response = await http.post(url, body: {"username":username.text.trim(), "password":password.text.trim(), "fullname": fullname.text.trim(), "gender": gender.text.trim()});
    if(response.statusCode == 200)
    {
      var userData = json.decode(response.body);
      if(userData == "ERROR")
      {
        showDialog(
            context: context,
            builder: (context)=> AlertDialog(
              title: Text("Thong bao"),
              content: Text("Tai khoan da ton tai!!!"),
              actions:<Widget> [
                RaisedButton(onPressed: (){
                  Navigator.pop(context);
                },
                  color: Colors.red,
                  child: Text("Ok"),
                ),
              ],
            ));
      }
      else{
        if(userData['status'] == "ADMIN")
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage(),),);
        }
        else
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(),),);
        }
        showDialog(
            context: context,
            builder: (context)=> AlertDialog(
              title: Text("Thong bao"),
              content: Text("Tai Tai Khoan Thanh cong!!!"),
              actions:<Widget> [
                RaisedButton(onPressed: (){
                  Navigator.pop(context);
                },
                  color: Colors.red,
                  child: Text("Ok"),
                ),
              ],
            ));
        print(userData);
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color.fromRGBO(135,206,250, 0.3),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
              child: Column(
                children:<Widget> [
                  Image(image: AssetImage("images/add_user.png"),height: 100,),
                  Text('Đăng Ký', style: TextStyle(color: Colors.amber, fontSize: 40),),
                  Card(
                    child: TextFormField(
                      controller: username,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong username":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Username",
                          hintText: "Enter username"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: fullname,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong Ten nguoi dung":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.account_balance, size: 30,),
                          ),
                          labelText: "Fullname",
                          hintText: "Enter fullname"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: password,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong password":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.lock, size: 30,),
                          ),
                          labelText: "Password",
                          hintText: "Enter Password"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: passwordconfirm,
                      validator: (values)=> passwordconfirm.text != passwordconfirm.text?"Khong khop":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.confirmation_num, size: 30,),
                          ),
                          labelText: "Password Confirm",
                          hintText: "Enter password confirm"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: gender,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong Gioi tinh":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Gender",
                          hintText: "Enter Gender"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  // ignore: deprecated_member_use

                  SizedBox(
                    height: 44,
                    // ignore: deprecated_member_use
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        onPressed: (){
                          signup();
                          //Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
                        },
                        color: Colors.lightBlueAccent,
                        child: Text(
                          "Register",
                          style: TextStyle(color: Colors.black,fontSize: 19),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Login here ->", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                      // ignore: deprecated_member_use
                      FlatButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
                      },
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.amber,
                              fontWeight: FontWeight.bold
                          ),
                        ),)
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}