import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../detailpost.dart';


class listItemPost extends StatefulWidget{
  final idplace;
  final nameplace;
  const listItemPost({Key? key, this.idplace, this.nameplace}) : super(key: key);

  _listItemPostState createState() => _listItemPostState();
}

class _listItemPostState extends State<listItemPost>{
  List lstpostBuyPlace = [];
  Future getPostByPlace()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/getPostBuyPlace.php");
    var response = await http.post(url, body: {'iddiadiem':widget.idplace});
    if(response.statusCode == 200 )
    {
      var jsonData =json.decode( response.body);
      setState(() {
        lstpostBuyPlace = jsonData;
      });
    }
  }
  @override
  void initState(){
    super.initState();
    getPostByPlace();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
      title: Text(widget.nameplace),
    ),
      body: Container(
        child: ListView.builder(
            itemCount: lstpostBuyPlace.length,
            itemBuilder: (context, index) {
              return  NewPostItem(
                iduser: lstpostBuyPlace[index]['fullname'],
                idpalce: lstpostBuyPlace[index]['idplace'],
                content: lstpostBuyPlace[index]['content'],
                comments: lstpostBuyPlace[index]['comments'],
                total_like: lstpostBuyPlace[index]['total_like'],
                imgpost: 'http://192.168.1.180/Flutter_Apptravel/upload/${lstpostBuyPlace[index]['imgpost']}',
                name: lstpostBuyPlace[index]['name'],
              );
            }),
      ),
    );
  }
}

class NewPostItem extends StatefulWidget{
  final name;
  final content;
  final imgpost;
  final comments;
  final total_like;
  final iduser;
  final idpalce;
  NewPostItem({Key? key, this.name, this.content, this.imgpost, this.comments, this.total_like, this.iduser, this.idpalce}) : super(key: key);
  @override
  _NewPostItemState createState() => _NewPostItemState();
}
class _NewPostItemState extends State<NewPostItem>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(image: NetworkImage(widget.imgpost)),
              // color: Colors.amber,
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blue,Colors.lightBlueAccent]
              ),
            ),
          ),
        ),
        // Container(
        //   height: 200,
        //   width: MediaQuery.of(context).size.width,
        //   decoration: BoxDecoration(
        //     color: Colors.amber,
        //     borderRadius: BorderRadius.circular(10),
        //     gradient: LinearGradient(
        //         begin: Alignment.topRight,
        //         end: Alignment.bottomLeft,
        //         colors: [Colors.amber,Colors.pink]
        //     ),
        //   ),
        //   // color: Colors.lightBlueAccent,
        // ),
        Positioned(
          top: 30,
          left: 40,
          child: CircleAvatar(
            radius: 20,
            child: Icon(Icons.person),
            // backgroundImage: NetworkImage(widget.imgpost),
          ),
        ),
        Positioned(
          top: 30,
          left: 90,
          child: Text(widget.iduser, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 50,
          left: 100,
          child: Icon(Icons.comment, color: Colors.white,),
        ),
        Positioned(
          top: 50,
          left: 140,
          child: Text("44", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 50,
          left: 170,
          child: Icon(Icons.label, color: Colors.white,),
        ),
        Positioned(
          top: 50,
          left: 200,
          child: Text(widget.total_like, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 100,
          left: 50,
          child: Text(widget.name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 146,
          left: 40,
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
        Positioned(
          top: 150,
          left: 80,
          child: InkWell(child: Text("Read More", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            onTap: (){
              Navigator.push( context, MaterialPageRoute(builder: (context) =>  PostDetail(
                name: widget.name, imgpost: widget.imgpost,content: widget.content, iduser: widget.iduser, idpalce: widget.idpalce,
              )),);
            },
          ),
        ),
      ],
    );
  }

}