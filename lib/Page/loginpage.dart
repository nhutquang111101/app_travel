import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

import '../admin/Dashboard.dart';
import '../main.dart';

class LoginPage extends StatefulWidget{
  @override
  _LoginPageState createState() => _LoginPageState();
}
class _LoginPageState extends State<LoginPage>{
  TextEditingController usernamelogin = new TextEditingController();
  TextEditingController passwordlogin = new TextEditingController();
  Future Login() async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/login.php");
    var response = await http.post(url, body: {"username":usernamelogin.text.trim(), "password":passwordlogin.text.trim()});
    if(response.statusCode == 200)
    {
      var userData = json.decode(response.body);
      if(userData == "ERROR")
      {
        showDialog(
            context: context,
            builder: (context)=> AlertDialog(
              title: Text("Thong bao"),
              content: Text("Tai khoan hoac mat khau ko dung!!!"),
              actions:<Widget> [
                RaisedButton(onPressed: (){
                  Navigator.pop(context);
                },
                  color: Colors.red,
                  child: Text("Ok"),
                ),
              ],
            ));
      }
      else{
        if(userData['status'] == "ADMIN")
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => DashboardPage(name: userData['fullname'],email: userData['username'], id: userData['idaccount'], ),),);
        }
        else
        {
          Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage(name: userData['fullname'], email: userData['username'],id: userData['idaccount']),),);
        }
        showDialog(
            context: context,
            builder: (context)=> AlertDialog(
              title: Text("Thong bao"),
              content: Text("Dang Nhap Thanh cong!!!"),
              actions:<Widget> [
                RaisedButton(onPressed: (){
                  Navigator.pop(context);
                },
                  color: Colors.red,
                  child: Text("Ok"),
                ),
              ],
            ));
        print(userData);
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(135,206,250, 0.3),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
              child: Column(
                children:<Widget> [
                  Image(image:
                  AssetImage("images/userlogo.png",),),
                  Card(
                    child: TextFormField(
                      controller: usernamelogin,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong username":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Username",
                          hintText: "Enter username"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: passwordlogin,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong password":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.phonelink_lock, size: 30,),
                          ),
                          labelText: "Password",
                          hintText: "Enter password"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  SizedBox(
                    height: 44,
                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      onPressed: (){
                        Login();
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> MyHomePage()));
                      },
                      color: Colors.lightBlueAccent,
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.black,fontSize: 19),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Have an account?", style: TextStyle(color: Colors.white),),
                      FlatButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Register()));
                      },
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                              color: Colors.amber,
                              fontWeight: FontWeight.bold
                          ),
                        ),)
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
