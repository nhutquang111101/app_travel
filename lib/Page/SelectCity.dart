import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'detailPlace.dart';

class SelectCityBy extends StatefulWidget{
  final idcity;
  final namecityOfplace;

  const SelectCityBy({Key? key, this.namecityOfplace, this.idcity}) : super(key: key);

  _SelectCityByState createState() => _SelectCityByState();
}

class _SelectCityByState extends State<SelectCityBy>{

  List lstplaceBuyCity = [];
  Future getPlaceByCity()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/listplaceById.php");
    var response = await http.post(url, body: {'id':widget.idcity});
    if(response.statusCode == 200 )
    {
      var jsonData =json.decode( response.body);
      setState(() {
        lstplaceBuyCity = jsonData;
      });
    }
  }
  @override
  void initState(){
    super.initState();
    getPlaceByCity();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.namecityOfplace),
      ),
      body: Container(
        child: ListView.builder(
            itemCount: lstplaceBuyCity.length,
            itemBuilder: (context, index) {
         return  NewPlaceItem(
           iddiadiem: lstplaceBuyCity[index]['iddiadiem'],
           userid: lstplaceBuyCity[index]['fullname'],
           idtinhthanh: lstplaceBuyCity[index]['name'],
           diachi: lstplaceBuyCity[index]['diachi'],
           tendiadiem: lstplaceBuyCity[index]['tendiadiem'],
           imgdiadiem: 'http://192.168.1.180/Flutter_Apptravel/upload/${lstplaceBuyCity[index]['imgdiadiem']}',
         );
        }),
      ),
    );
  }

}

class NewPlaceItem extends StatefulWidget{
  final iddiadiem;
  final tendiadiem;
  final diachi;
  final imgdiadiem;
  final userid;
  final idtinhthanh;
  NewPlaceItem({Key? key,this.iddiadiem, this.tendiadiem, this.diachi, this.imgdiadiem, this.userid, this.idtinhthanh}) : super(key: key);
  @override
  _NewPlaceItemState createState() => _NewPlaceItemState();
}
class _NewPlaceItemState extends State<NewPlaceItem>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(image: NetworkImage(widget.imgdiadiem)),
              // color: Colors.amber,
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blue,Colors.lightBlueAccent]
              ),
            ),
          ),
        ),
        // Container(
        //   height: 200,
        //   width: MediaQuery.of(context).size.width,
        //   decoration: BoxDecoration(
        //     color: Colors.amber,
        //     borderRadius: BorderRadius.circular(10),
        //     gradient: LinearGradient(
        //         begin: Alignment.topRight,
        //         end: Alignment.bottomLeft,
        //         colors: [Colors.amber,Colors.pink]
        //     ),
        //   ),
        //   // color: Colors.lightBlueAccent,
        // ),
        Positioned(
          top: 20,
          left: 70,
          child: CircleAvatar(
            radius: 20,
            child: Icon(Icons.person),
            // backgroundImage: NetworkImage(widget.imgpost),
          ),
        ),
        Positioned(
          top: 30,
          left: 130,
          child: Text(widget.userid.toString(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),),
        ),
        Positioned(
          top: 70,
          left: 70,
          child: Icon(Icons.place, color: Colors.white,),
        ),

        Positioned(
          top: 70,
          left: 100,
          child: Text(widget.tendiadiem, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),),
        ),
        Positioned(
          top: 146,
          left: 40,
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
        Positioned(
          top: 150,
          left: 80,
          child: InkWell(child: Text("Read More", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            onTap: (){
              Navigator.push( context, MaterialPageRoute(builder: (context) =>  PlaceDetail(iddiadiem: widget.iddiadiem,
                tendiadiem: widget.tendiadiem, imgdiadiem: widget.imgdiadiem,diachi: widget.diachi, userid: widget.userid, idtinhthanh: widget.idtinhthanh,
              )),);
            },
          ),
        ),
      ],
    );
  }

}