import 'package:flutter/material.dart';

class PostDetail extends StatelessWidget{
  final name;
  final content;
  final imgpost;
  final iduser;
  final idpalce;

  PostDetail({Key? key, this.name, this.content, this.imgpost, this.iduser, this.idpalce}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Container(
          margin: EdgeInsets.only(left: 50),
          child: Text("Chi Tiết bài Viết", style: TextStyle(fontWeight: FontWeight.bold),),
        ),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(8.0),
            child: Text(name, style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),),
            ),
            SizedBox(height: 20,),
            Container( child: Image.network(imgpost),),
            SizedBox(height: 20,),
            Padding(padding: EdgeInsets.all(8.0),
              child: Text(content, style: TextStyle(
                fontSize: 18,
              ),),
            ),
            SizedBox(height: 20,),
            Padding(padding: EdgeInsets.all(8.0),
              child: Text("Tạo Bởi: "+ iduser, style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              width: MediaQuery.of(context).size.width,
              height: 186,
              decoration: BoxDecoration(color: Colors.white,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)), boxShadow: [
                    BoxShadow(color: Colors.grey.withOpacity(0.8), //color of shadow
                      spreadRadius: 5, //spread radius
                      blurRadius: 7, // blur radius
                      offset: Offset(0, 2),),
                  ]),
              child: Container(
                margin: EdgeInsets.only(left: 1, top: 10),
                child:
                Column(
                  children: [
                    Text("Bình Luận",
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: SizedBox( // <-- SEE HERE
                        width: 300,
                        child: TextField(
                          decoration: InputDecoration(
                            labelText: 'Nhập Bình luận',
                            border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: MaterialButton(
                        onPressed: (){},
                        color: Colors.lightBlue,
                        child: Text("Đăng"),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}