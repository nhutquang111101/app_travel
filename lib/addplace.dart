import 'dart:convert';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:untitled1/main.dart';
import 'package:untitled1/widgets/header_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'listcity.dart';

class AddPage extends StatefulWidget{

   AddPage({Key? key}) : super (key: key);
  // final List list;
  // final int index;
  //  AddPage({required this.list, required this.index});
  @override
  State<StatefulWidget> createState() {
    return _AddPageState();
  }
}

class _AddPageState extends State<AddPage>{

  double _drawerIconSize = 24;
  double _drawerFontSize = 17;
  TextEditingController nameCity = new TextEditingController();
  bool editMode = false;
  Future addCity()async{
    if(editMode == true)
    {
    }
    else
    {
      var urlAddcity =Uri.parse("http://192.168.1.180/Flutter_Apptravel/addcity.php");
      var response = await http.post(urlAddcity, body: {
        "name" : nameCity.text,
      });
      var data = json.decode(response.body);
      if(data == "Error")
      {
        Fluttertoast.showToast(
            msg: "Tao Khong Thanh Cong",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
      else
      {
        Fluttertoast.showToast(
            msg: "Tao Thanh Cong",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0
        );
      }
    }


}
//   @override
//   void initState(){
//       if(widget.index != null)
//       {
//         editMode = true;
//         nameCity.text = widget.list[widget.index]['name'];
//       }
//       super.initState();
//   }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(editMode ? "Update Place" : "Add Place",
          style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold),
        ),
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.white),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).colorScheme.secondary]
              )
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(top: 16, right: 16),
            child: Stack(
              children: <Widget>[
                Icon(Icons.notifications),
                Positioned(
                  right: 0,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(6),),
                    constraints: BoxConstraints(minWidth: 12, minHeight: 12,),
                    child: Text('5', style: TextStyle(color: Colors.white, fontSize: 8,), textAlign: TextAlign.center,),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [0.0,1.0],
                  colors: [
                    Theme.of(context).primaryColor.withOpacity(0.1),
                    Theme.of(context).colorScheme.secondary.withOpacity(0.4)
                  ]
              )
          ),
          child: ListView(
            children: [
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.0, 1.0],
                    colors: [ Theme.of(context).primaryColor,Theme.of(context).accentColor,],
                  ),
                ),
                child: Container(
                  alignment: Alignment.bottomLeft,
                  child: Text("Admin UI",
                    style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.place, size: _drawerIconSize, color: Theme.of(context).colorScheme.secondary),
                title: Text('Thêm Tỉnh Thành', style: TextStyle(fontSize: 17, color: Theme.of(context).colorScheme.secondary),),
                onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddPage()));
                },
              ),
              ListTile(
                leading: Icon(Icons.list_alt,size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary),
                title: Text('Danh Sách Tỉnh Thành', style: TextStyle(fontSize: _drawerFontSize, color: Theme.of(context).colorScheme.secondary),
                ),
                onTap: () {
                   Navigator.push(context, MaterialPageRoute(builder: (context) => listcity()),);
                },
              ),
              Divider(color: Theme.of(context).primaryColor, height: 1,),
              ListTile(
                leading: Icon(Icons.person_add_alt_1, size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary),
                title: Text('trang Chủ',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).colorScheme.secondary),),
                onTap: () {
                   Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage()),);
                },
              ),


              Divider(color: Theme.of(context).primaryColor, height: 1,),
              ListTile(
                leading: Icon(Icons.logout_rounded, size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary,),
                title: Text('Logout',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).colorScheme.secondary),),
                onTap: () {
                  SystemNavigator.pop();
                },
              ),
            ],
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child:  Text("Thêm Tỉnh Thành", style: TextStyle(color: Colors.green, fontSize: 40, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(1),
                  child: Image.asset(
                      'images/addplace.jpg',
                      width: 200,
                      height: 200,
                      fit:BoxFit.fill
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: SizedBox( // <-- SEE HERE
                    width: 300,
                    child: TextField(
                      controller: nameCity,
                      decoration: InputDecoration(
                        labelText: 'Nhập tên Tỉnh Thành',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: SizedBox( // <-- SEE HERE
                    width: 200,
                    child:RaisedButton(
                      onPressed: (){
                        setState(() {
                          addCity();
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> listcity()));
                        });
                      },color: Colors.lightBlueAccent,
                      child: Text(editMode ? "Sua Dia Diem" :
                        "Thêm Địa điểm",
                        style: TextStyle(color: Colors.black,fontSize: 19),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



///==============================
class UpdatePage extends StatefulWidget{

  // AddPage({Key? key, required this.list, required this.index}) : super (key: key);
  final List list;
  final int index;
  UpdatePage({required this.list, required this.index});
  @override
  State<StatefulWidget> createState() {
    return _UpdatePageState();
  }
}

class _UpdatePageState extends State<UpdatePage>{

  double _drawerIconSize = 24;
  double _drawerFontSize = 17;
  TextEditingController nameCity = new TextEditingController();
  bool editMode = false;
  Future UpdateCity()async{

      var urlEdit = Uri.parse("http://192.168.1.180/Flutter_Apptravel/update.php");
      var response = await http.post(urlEdit, body:{
        "id" : widget.list[widget.index]['id'],
        "name" : nameCity.text,
      }
      );
      var data = json.decode(response.body);
    }

  @override
  void initState(){
    if(widget.index != null)
    {
      editMode = true;
      nameCity.text = widget.list[widget.index]['name'];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(editMode ? "Update Place" : "Add Place",
          style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold),
        ),
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.white),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).colorScheme.secondary]
              )
          ),
        ),
        actions: [
          Container(
            margin: EdgeInsets.only(top: 16, right: 16),
            child: Stack(
              children: <Widget>[
                Icon(Icons.notifications),
                Positioned(
                  right: 0,
                  child: Container(
                    padding: EdgeInsets.all(1),
                    decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(6),),
                    constraints: BoxConstraints(minWidth: 12, minHeight: 12,),
                    child: Text('5', style: TextStyle(color: Colors.white, fontSize: 8,), textAlign: TextAlign.center,),
                  ),
                )
              ],
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(
                  child:  Text("Sửa Tỉnh Thành phố", style: TextStyle(color: Colors.green, fontSize: 40, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(1),
                  child: Image.asset(
                      'images/edit.jpg',
                      width: 200,
                      height: 200,
                      fit:BoxFit.fill
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: SizedBox( // <-- SEE HERE
                    width: 300,
                    child: TextField(
                      controller: nameCity,
                      decoration: InputDecoration(
                        labelText: 'Nhập tên Tỉnh Thành',
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: SizedBox( // <-- SEE HERE
                    width: 200,
                    child:RaisedButton(
                      onPressed: (){
                        UpdateCity();
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> listcity()));
                      },color: Colors.lightBlueAccent,
                      child: Text(editMode ? "Sửa " :
                      "Thêm Địa điểm",
                        style: TextStyle(color: Colors.black,fontSize: 19),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}