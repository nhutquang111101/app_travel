import 'package:flutter/material.dart';
import 'package:untitled1/inputwrapper.dart';
import 'package:untitled1/main.dart';

class AddPostScreen extends StatefulWidget{
  _AddPostScreenState createState() => _AddPostScreenState();
}
class _AddPostScreenState extends State<AddPostScreen>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
        child: Scaffold(
          appBar:AppBar(
            title: Container(
              margin: EdgeInsets.only(left: 70),
              child: Text("Tạo bài viết", style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold),),
            ),
            leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>MyHomePage()));
            },),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: Column(
                children: [
                  Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10, top: 20),
                        child: Row(
                          children: [
                            CircleAvatar(
                              child: IconButton(onPressed: (){}, icon: Icon(Icons.person)),

                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child:
                              Text("Your name", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20),
                        child: SizedBox( // <-- SEE HERE
                          width: 350,
                          child: TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: 10,
                            decoration: InputDecoration(
                              labelText: 'Nhập Nội Dung Bài Viết',
                              border: OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        child: Container(
                          margin: EdgeInsets.only(top: 250),
                            width: MediaQuery.of(context).size.width,
                            height: 100,
                            decoration: BoxDecoration(color: Colors.white,
                                borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)), boxShadow: [
                                  BoxShadow(color: Colors.grey.withOpacity(0.8), //color of shadow
                                    spreadRadius: 5, //spread radius
                                    blurRadius: 7, // blur radius
                                    offset: Offset(0, 2),),
                                ]),
                          child: Column(
                            children: [
                                IconButton(onPressed: (){}, icon: Icon(Icons.photo_camera_back_rounded, size: 40,color: Colors.lightGreen,)),
                              Container(

                                child: ElevatedButton(
                                    onPressed: (){
                                    },
                                    child: Text("Đăng bài")
                                ),
                              ),
                            ],
                          )),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
    );
  }

}