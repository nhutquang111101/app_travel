import 'dart:convert';

// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:untitled1/detailpost.dart';
import 'package:untitled1/main.dart';

class TopPostCard extends StatefulWidget{
  @override
  _TopPostCardState createState() =>_TopPostCardState();
}

class _TopPostCardState extends State<TopPostCard>{

  //tao 1 listr rong
  List postData = [];
  Map<String, String> headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Charset': 'utf-8'
  };

  Future showAllPost()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/getPost.php");
    var response = await http.get(url, headers: headers);
      var jsonData = json.decode(response.body);
      setState(() {
        postData = jsonData;
      });
      print(jsonData);
      return jsonData;
  }
  @override
  void initState()
  {
    super.initState();
    showAllPost();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      //color: Colors.amber,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: postData.length,
          itemBuilder: (context,index){
            return NewPostItem(
              iduser: postData[index]['fullname'],
              idpalce: postData[index]['idplace'],
              content: postData[index]['content'],
              comments: postData[index]['comments'],
              total_like: postData[index]['total_like'],
              imgpost: 'http://192.168.1.180/Flutter_Apptravel/upload/${postData[index]['imgpost']}',
              name: postData[index]['name'],
            );
          }),
    );
  }
}

class NewPostItem extends StatefulWidget{
  final name;
  final content;
  final imgpost;
  final comments;
  final total_like;
  final iduser;
  final idpalce;
  NewPostItem({Key? key, this.name, this.content, this.imgpost, this.comments, this.total_like, this.iduser, this.idpalce}) : super(key: key);
  @override
  _NewPostItemState createState() => _NewPostItemState();
}
class _NewPostItemState extends State<NewPostItem>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(image: NetworkImage(widget.imgpost)),
              // color: Colors.amber,
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blue,Colors.lightBlueAccent]
              ),
            ),
          ),
        ),
        // Container(
        //   height: 200,
        //   width: MediaQuery.of(context).size.width,
        //   decoration: BoxDecoration(
        //     color: Colors.amber,
        //     borderRadius: BorderRadius.circular(10),
        //     gradient: LinearGradient(
        //         begin: Alignment.topRight,
        //         end: Alignment.bottomLeft,
        //         colors: [Colors.amber,Colors.pink]
        //     ),
        //   ),
        //   // color: Colors.lightBlueAccent,
        // ),
        Positioned(
          top: 30,
          left: 40,
          child: CircleAvatar(
            radius: 20,
            child: Icon(Icons.person),
            // backgroundImage: NetworkImage(widget.imgpost),
          ),
        ),
        Positioned(
          top: 30,
          left: 90,
          child: Text(widget.iduser, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 50,
          left: 100,
          child: Icon(Icons.comment, color: Colors.white,),
        ),
        Positioned(
          top: 50,
          left: 140,
          child: Text("44", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 50,
          left: 170,
          child: Icon(Icons.label, color: Colors.white,),
        ),
        Positioned(
          top: 50,
          left: 200,
          child: Text(widget.total_like, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 100,
          left: 50,
          child: Text(widget.name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        ),
        Positioned(
          top: 146,
          left: 40,
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
        Positioned(
          top: 150,
          left: 80,
          child: InkWell(child: Text("Read More", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            onTap: (){
              Navigator.push( context, MaterialPageRoute(builder: (context) =>  PostDetail(
                name: widget.name, imgpost: widget.imgpost,content: widget.content, iduser: widget.iduser, idpalce: widget.idpalce,
              )),);
            },
          ),
        ),
      ],
    );
  }

}