import 'dart:convert';

// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:untitled1/Page/detailPlace.dart';
import 'package:untitled1/detailpost.dart';
import 'package:untitled1/main.dart';

class TopPlaceCard extends StatefulWidget{
  @override
  _TopPlaceCardState createState() =>_TopPlaceCardState();
}

class _TopPlaceCardState extends State<TopPlaceCard>{

  //tao 1 listr rong
  List placeData = [];
  Map<String, String> headers = {
    'Content-Type': 'application/json;charset=UTF-8',
    'Charset': 'utf-8'
  };

  Future showAllPost()async{
    var url = Uri.parse("http://192.168.1.180/Flutter_Apptravel/upload/getPlaceByCity.php");
    var response = await http.get(url, headers: headers);
    var jsonData = json.decode(response.body);
    setState(() {
      placeData = jsonData;
    });
    print(jsonData);
    return jsonData;
  }
  @override
  void initState()
  {
    super.initState();
    showAllPost();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      //color: Colors.amber,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: placeData.length,
          itemBuilder: (context,index){
            return NewPlaceItem(
              iddiadiem: placeData[index]['iddiadiem'],
              userid: placeData[index]['fullname'],
              idtinhthanh: placeData[index]['name'],
              diachi: placeData[index]['diachi'],
              tendiadiem: placeData[index]['tendiadiem'],
              imgdiadiem: 'http://192.168.1.180/Flutter_Apptravel/upload/${placeData[index]['imgdiadiem']}',
            );
          }),
    );
  }
}

class NewPlaceItem extends StatefulWidget{
  final iddiadiem;
  final tendiadiem;
  final diachi;
  final imgdiadiem;
  final userid;
  final idtinhthanh;
  NewPlaceItem({Key? key,this.iddiadiem, this.tendiadiem, this.diachi, this.imgdiadiem, this.userid, this.idtinhthanh}) : super(key: key);
  @override
  _NewPlaceItemState createState() => _NewPlaceItemState();
}
class _NewPlaceItemState extends State<NewPlaceItem>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.all(8.0),
          child: Container(
            height: 200,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(image: NetworkImage(widget.imgdiadiem)),
              // color: Colors.amber,
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [Colors.blue,Colors.lightBlueAccent]
              ),
            ),
          ),
        ),
        // Container(
        //   height: 200,
        //   width: MediaQuery.of(context).size.width,
        //   decoration: BoxDecoration(
        //     color: Colors.amber,
        //     borderRadius: BorderRadius.circular(10),
        //     gradient: LinearGradient(
        //         begin: Alignment.topRight,
        //         end: Alignment.bottomLeft,
        //         colors: [Colors.amber,Colors.pink]
        //     ),
        //   ),
        //   // color: Colors.lightBlueAccent,
        // ),
        Positioned(
          top: 20,
          left: 70,
          child: CircleAvatar(
            radius: 20,
            child: Icon(Icons.person),
            // backgroundImage: NetworkImage(widget.imgpost),
          ),
        ),
        Positioned(
          top: 30,
          left: 130,
          child: Text(widget.userid, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 16),),
        ),
        Positioned(
          top: 70,
          left: 70,
          child: Icon(Icons.place, color: Colors.white,),
        ),

        Positioned(
          top: 70,
          left: 100,
          child: Text(widget.tendiadiem, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),),
        ),
        Positioned(
          top: 146,
          left: 40,
          child: Icon(Icons.arrow_back, color: Colors.white,),
        ),
        Positioned(
          top: 150,
          left: 80,
          child: InkWell(child: Text("Read More", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            onTap: (){
              Navigator.push( context, MaterialPageRoute(builder: (context) =>  PlaceDetail(iddiadiem: widget.iddiadiem,
                tendiadiem: widget.tendiadiem, imgdiadiem: widget.imgdiadiem,diachi: widget.diachi, userid: widget.userid, idtinhthanh: widget.idtinhthanh,
              )),);
            },
          ),
        ),
      ],
    );
  }

}