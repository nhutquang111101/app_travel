import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:untitled1/Page/loginpage.dart';
import 'package:untitled1/addplace.dart';
import 'package:untitled1/profile.dart';
import 'package:untitled1/style.dart';
import 'package:untitled1/widgets/TopPlaceCard.dart';
import 'package:untitled1/widgets/TopPostCard.dart';
import 'package:untitled1/widgets/addpost.dart';
import 'CityListItem.dart';
import 'Header.dart';
import 'Page/signuppage.dart';
import 'inputwrapper.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';

import 'listcity.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MainScreen(),
    );
  }
}


class MainScreen extends StatelessWidget {
  // const MainScreen({Key? key}) : super (key: key);
  @override
  Widget build(BuildContext context) {
return Scaffold(
  body: MyHomePage(),
);

  }
}
 class FirstScreen extends StatelessWidget {
   const FirstScreen({Key? key}) : super (key: key);
   @override
   Widget build(BuildContext context)
   {
     double _drawerIconSize = 24;
     double _drawerFontSize = 17;
     return Container(
       color: Colors.white,
       child: SafeArea(
         child: Scaffold(
           appBar: AppBar(
             title: Text("Home",
               style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold),
             ),
             elevation: 0.5,
             iconTheme: IconThemeData(color: Colors.white),
             flexibleSpace: Container(
               decoration: BoxDecoration(
                   gradient: LinearGradient(
                       begin: Alignment.topLeft,
                       end: Alignment.bottomRight,
                       colors: <Color>[Theme.of(context).primaryColor, Theme.of(context).colorScheme.secondary]
                   )
               ),
             ),
             actions: [
               Container(
                 margin: EdgeInsets.only(top: 16, right: 16),
                 child: Stack(
                   children: <Widget>[
                     Icon(Icons.person),
                     // Positioned(
                     //   right: 0,
                     //   child: Container(
                     //     padding: EdgeInsets.all(1),
                     //     decoration: BoxDecoration(color: Colors.red, borderRadius: BorderRadius.circular(6),),
                     //     constraints: BoxConstraints(minWidth: 12, minHeight: 12,),
                     //     child: Text('5', style: TextStyle(color: Colors.white, fontSize: 8,), textAlign: TextAlign.center,),
                     //   ),
                     // )
                   ],
                 ),
               )
             ],
           ),
           drawer: Drawer(
             child: Container(
               decoration: BoxDecoration(
                   gradient: LinearGradient(
                       begin: Alignment.topLeft,
                       end: Alignment.bottomRight,
                       stops: [0.0,1.0],
                       colors: [
                         Theme.of(context).primaryColor.withOpacity(0.1),
                         Theme.of(context).colorScheme.secondary.withOpacity(0.4)
                       ]
                   )
               ),
               child: ListView(
                 children: [
                   DrawerHeader(
                     decoration: BoxDecoration(
                       color: Theme.of(context).primaryColor,
                       gradient: LinearGradient(
                         begin: Alignment.topLeft,
                         end: Alignment.bottomRight,
                         stops: [0.0, 1.0],
                         colors: [ Theme.of(context).primaryColor,Theme.of(context).accentColor,],
                       ),
                     ),
                     child: Container(
                       alignment: Alignment.bottomLeft,
                       child: Text("Admin place",
                         style: TextStyle(fontSize: 25,color: Colors.white, fontWeight: FontWeight.bold),
                       ),
                     ),
                   ),
                   ListTile(
                     leading: Icon(Icons.screen_lock_landscape_rounded, size: _drawerIconSize, color: Theme.of(context).colorScheme.secondary),
                     title: Text('About me', style: TextStyle(fontSize: 17, color: Theme.of(context).colorScheme.secondary),),
                     onTap: (){
                       // Navigator.push(context, MaterialPageRoute(builder: (context) => SplashScreen(title: "Splash Screen")));
                     },
                   ),
                   ListTile(
                     leading: Icon(Icons.login_rounded,size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary),
                     title: Text('Login Page', style: TextStyle(fontSize: _drawerFontSize, color: Theme.of(context).colorScheme.secondary),
                     ),
                     onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()),);
                     },
                   ),
                   Divider(color: Theme.of(context).primaryColor, height: 1,),
                   ListTile(
                     leading: Icon(Icons.person_add_alt_1, size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary),
                     title: Text('Registration Page',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).colorScheme.secondary),),
                     onTap: () {
                       Navigator.push(context, MaterialPageRoute(builder: (context) => Register()),);
                     },
                   ),
                   Divider(color: Theme.of(context).primaryColor, height: 1,),
                   ListTile(
                     leading: Icon(Icons.password_rounded, size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary,),
                     title: Text('Add Place',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).colorScheme.secondary),),
                     onTap: () {
                       // Navigator.push( context, MaterialPageRoute(builder: (context) => AddPage(list: list, index: index)),);
                     },
                   ),

                   Divider(color: Theme.of(context).primaryColor, height: 1,),
                   ListTile(
                     leading: Icon(Icons.logout_rounded, size: _drawerIconSize,color: Theme.of(context).colorScheme.secondary,),
                     title: Text('Logout',style: TextStyle(fontSize: _drawerFontSize,color: Theme.of(context).colorScheme.secondary),),
                     onTap: () {
                       SystemNavigator.pop();
                     },
                   ),
                 ],
               ),
             ),
           ),
           body: SingleChildScrollView(
             child: Padding(
               padding: const EdgeInsets.all(10.0),
               child: Column(
                 children: [
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [
                        const Text(
                          '',
                          style: greyStyle,
                        ),
                     ],
                   ),
                   SizedBox(
                     width: double.infinity,
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: const [
                         Text("khám Phá", style: bigBlackTitleStyle,

                         ),
                         Text("Tận Hưởng", style: bigBlackTitleStyle,

                         ),
                       ],
                     ),
                   ),
                   const SizedBox(height: 10),
                   Row(
                     children: [
                       Expanded(
                         flex: 5,
                           child: Card(
                             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                             child: TextFormField(
                               style: TextStyle(fontSize: 20),
                               decoration: InputDecoration(
                                   contentPadding: EdgeInsets.all(8),
                                   prefixIcon: Padding(
                                     padding: EdgeInsets.only(left: 20, right:15),
                                     child: Icon(Icons.search, size: 30,),
                                   ),
                                   hintText: "Search Here",
                               ),
                               keyboardType: TextInputType.text,
                             ),
                           ),
                       ),
                      // const SizedBox(width: 5,),
                      //  Expanded(
                      //    flex: 1,
                      //      child: Card(
                      //        color: Colors.grey[200],
                      //        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                      //        child: Padding(
                      //          padding: const EdgeInsets.all(10),
                      //          child: Icon(
                      //            Icons.menu,
                      //            color: Colors.grey[700],
                      //            size: 30,
                      //          ),
                      //        ),
                      //      ),
                      //  ),
                     ],
                   ),

                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceAround,
                     children: [
                       Column(
                         children: [
                           Text('Các Điểm Du Lịch', style: blackStyle,),
                           Container(
                             width: 10,
                             height: 10,
                             decoration: BoxDecoration(
                               color: Colors.black,
                               borderRadius: BorderRadius.circular(50),
                             ),
                           ),
                         ],
                       ),
                     ],
                   ),
                   SizedBox(height: 25),
                   SingleChildScrollView(
                     scrollDirection: Axis.horizontal,
                     child: Row(
                       children: [
                         Stack(
                            children: [
                              ClipRRect(
                                  borderRadius: BorderRadius.circular(35),
                                  child: Image.asset("img1.jpg",
                                    height: 220,
                                    width: 180,
                                    fit: BoxFit.cover,)
                              ),
                              Positioned(
                                top: 10,
                                right: 10,
                                child: Container(
                                  height: 50,
                                  width: 50,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Colors.white54,
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Column(
                                    children: [
                                      Icon(Icons.star, color: Colors.amber, size: 30,),
                                      Text('4.2', style: blackStyle,),
                                    ],
                                  ),
                                ),
                              ),

                              Positioned(
                                bottom: 10,
                                left: 10,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    // Text("Nha Trang", style: whiteTitleStyle,),
                                    TextButton(onPressed: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>SecondScreen()));
                                    }, child: Text("Nha Trang", style: whiteTitleStyle,),)
                                  ],
                                ),
                              ),

                            ],
                          ),
                         SizedBox(width: 15),
                         Stack(
                           children: [

                             ClipRRect(
                                 borderRadius: BorderRadius.circular(35),
                                 child: Image.asset("img2.jpg",
                                   height: 220,
                                   width: 180,
                                   fit: BoxFit.cover,)
                             ),
                             Positioned(
                               top: 10,
                               right: 10,
                               child: Container(
                                 height: 50,
                                 width: 50,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   color: Colors.white54,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                                 child: Column(
                                   children: [
                                     Icon(Icons.star, color: Colors.amber, size: 30,),
                                     Text('4.2', style: blackStyle,),
                                   ],
                                 ),
                               ),
                             ),

                             Positioned(
                               bottom: 10,
                               left: 10,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Text("Phu Quoc", style: whiteTitleStyle,),
                                 ],
                               ),
                             ),

                           ],
                         ),
                         SizedBox(width: 15),
                         Stack(
                           children: [

                             ClipRRect(
                                 borderRadius: BorderRadius.circular(35),
                                 child: Image.asset("img3.jpg",
                                   height: 220,
                                   width: 180,
                                   fit: BoxFit.cover,)
                             ),
                             Positioned(
                               top: 10,
                               right: 10,
                               child: Container(
                                 height: 50,
                                 width: 50,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   color: Colors.white54,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                                 child: Column(
                                   children: [
                                     Icon(Icons.star, color: Colors.amber, size: 30,),
                                     Text('4.2', style: blackStyle,),
                                   ],
                                 ),
                               ),
                             ),

                             Positioned(
                               bottom: 10,
                               left: 10,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Text("Phan thiet", style: whiteTitleStyle,),
                                 ],
                               ),
                             ),

                           ],
                         ),
                         SizedBox(width: 15),
                         Stack(
                           children: [

                             ClipRRect(
                                 borderRadius: BorderRadius.circular(35),
                                 child: Image.asset("img1.jpg",
                                   height: 220,
                                   width: 180,
                                   fit: BoxFit.cover,)
                             ),
                             Positioned(
                               top: 10,
                               right: 10,
                               child: Container(
                                 height: 50,
                                 width: 50,
                                 alignment: Alignment.center,
                                 decoration: BoxDecoration(
                                   color: Colors.white54,
                                   borderRadius: BorderRadius.circular(15),
                                 ),
                                 child: Column(
                                   children: [
                                     Icon(Icons.star, color: Colors.amber, size: 30,),
                                     Text('4.2', style: blackStyle,),
                                   ],
                                 ),
                               ),
                             ),

                             Positioned(
                               bottom: 10,
                               left: 10,
                               child: Column(
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [
                                   Text("Hai Phong", style: whiteTitleStyle,),
                                 ],
                               ),
                             ),

                           ],
                         ),
                       ],
                     ),
                   ),

                   const SizedBox(height: 25),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: const [
                          Text("Cảm hứng du lịch", style: blackStyle,),
                          Text("Xem", style: greyStyle,),
                     ],
                   ),
                   SizedBox(height: 15,),
                   Container(
                     padding: EdgeInsets.all(10),
                     decoration: BoxDecoration(color: Colors.grey[200],
                     borderRadius: BorderRadius.circular(20),
                     ),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                        Row(
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: Image.asset("img2.jpg", width: 90, height: 90, fit: BoxFit.cover,),),
                            Column(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Đảo Phú Quốc", style: blackStyle),
                                Text("Giá Tham Khảo", style: blackStyle),
                                Row(
                                  children: [
                                    Icon(Icons.star, color: Colors.amber,),
                                    Icon(Icons.star, color: Colors.amber,),
                                    Icon(Icons.star, color: Colors.amber,),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                          const Text("3.5",style: blackStyle,),
                       ],
                     ),
                   ),
                 ],
               ),
             ),
           ),
         ),
       ),
     );
   }
 }

 class SecondScreen extends StatelessWidget{
   // const SecondScreen({Key? key}) : super (key: key);

   @override
   Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.blue,
      child: SafeArea(
        child: Scaffold(
         body: Stack(
           children: [
             Image.asset('img1.jpg', height: MediaQuery.of(context).size.height, width: MediaQuery.of(context).size.width, fit: BoxFit.cover,),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                      color: Colors.white,
                      child: IconButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>MyHomePage()));
                      }, icon: Icon(Icons.arrow_back)),
                    ),

                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                      color: Colors.white,
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Icon(Icons.favorite_outline),
                      ),
                    ),
                  ],
                ),
              ),

             Positioned(
               bottom: 0,
               child: Container(
                 width: MediaQuery.of(context).size.width,
                 height: 320,
                 decoration: BoxDecoration(color: Colors.white,
                     borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25))),
                 child: Column(
                   children: [
                     Padding(
                       padding: const EdgeInsets.all(15),
                       child: Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                         children: [
                           Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                               children: const [
                               Text(
                                 'Đảo Phú Quốc',
                                 style: bigBlackTitleStyle,

                               ),
                               Text(
                                 'Kiên Giang, Việt Nam',
                                 style: greyStyle,
                               ),
                             ],
                           ),
                           Column(
                             children: [
                               Icon(Icons.star, color: Colors.amber, size: 35,),
                               Text('4.5', style: blackStyle,),
                             ],
                           ),

                         ],
                       ),
                     ),
                     Padding(
                       padding: const EdgeInsets.all(15),
                       child: Row(
                         children: [
                           Text('500.000VND', style: titleStyle,),
                           Text('/Đêm', style: titleStyle,),
                         ],
                       ),
                     ),
                     Padding(
                       padding: const EdgeInsets.symmetric(horizontal: 20),
                       child: Text(
                           'Phú Quốc từ lâu đã nổi tiếng với du khách từ mọi miền đất nước và cả khách du lịch quốc tế. Không chỉ là hòn đảo xinh đẹp với phong cảnh thiên nhiên yên bình, hoang sơ, '
                         ,
                         style: greyStyle,
                       ),
                     ),
                    SizedBox(height: 10,),
                     ElevatedButton(
                       style: ElevatedButton.styleFrom(
                         alignment: Alignment.center,
                         primary: Colors.deepPurpleAccent,
                         elevation: 5,
                         shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50),),
                         padding: EdgeInsets.symmetric(vertical: 20, horizontal: 130),
                       ),
                       onPressed: (){},
                       child: Text('Xem Ngay',
                         style: TextStyle(color: Colors.white,fontSize: 18),
                       ),
                     ),
                   ],
                 ),
               ),
             ),
           ],
         ),
        ),
      ),
    );
  }
 }

 class ThirdScreen extends StatelessWidget{
   const ThirdScreen({Key? key}) : super(key: key);
   @override
   Widget build(BuildContext context)
   {
     return Container(
       color: Colors.blue,
       child: SafeArea(
         child: Scaffold(
           body: Column(
             children: [
               SizedBox(width: double.infinity, height: 50,),
               Row(
                 children: [
                   Card(
                     elevation: 5,
                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                     color: Colors.blue,
                     child: const Padding(
                       padding: EdgeInsets.all(8.0),
                       child: Icon(Icons.arrow_back),
                     ),
                   ),
                  Container(
                    margin: EdgeInsets.only(left: 90),
                    child:  Text('Bộ Sưu tập', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                  ),
                 ],
               ),
               Container(
                 margin: EdgeInsets.only(top: 10),
                 child: Column(
                   children: [
                     Row(
                       children: [
                         SizedBox(width: 15),
                         Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
                           children: [
                             Text('Tất Cả đã yêu thích', style: blackStyle,),
                             Text('10 bài viết'),
                           ],
                         ),
                       ],
                     ),
                     SizedBox(height: 50,),
                     Container(
                       margin: EdgeInsets.only(left: 10),
                       child: SingleChildScrollView(
                         scrollDirection: Axis.horizontal,
                         child: Column(
                           children: [
                             Row(
                               children: [
                                 Stack(
                                   children: [

                                     ClipRRect(
                                         borderRadius: BorderRadius.circular(35),
                                         child: Image.asset("img1.jpg",
                                           height: 220,
                                           width: 180,
                                           fit: BoxFit.cover,)
                                     ),
                                     Positioned(
                                       top: 10,
                                       right: 10,
                                       child: Container(
                                         height: 50,
                                         width: 50,
                                         alignment: Alignment.center,
                                         decoration: BoxDecoration(
                                           color: Colors.white54,
                                           borderRadius: BorderRadius.circular(15),
                                         ),
                                         child: Column(
                                           children: [
                                             Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                             Text('4.2', style: blackStyle,),
                                           ],
                                         ),
                                       ),
                                     ),

                                     Positioned(
                                       bottom: 10,
                                       left: 10,
                                       child: Column(
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           Text("Nha Trang", style: whiteTitleStyle,),
                                         ],
                                       ),
                                     ),

                                   ],
                                 ),
                                 SizedBox(width: 15),
                                 Stack(
                                   children: [

                                     ClipRRect(
                                         borderRadius: BorderRadius.circular(35),
                                         child: Image.asset("img1.jpg",
                                           height: 220,
                                           width: 180,
                                           fit: BoxFit.cover,)
                                     ),
                                     Positioned(
                                       top: 10,
                                       right: 10,
                                       child: Container(
                                         height: 50,
                                         width: 50,
                                         alignment: Alignment.center,
                                         decoration: BoxDecoration(
                                           color: Colors.white54,
                                           borderRadius: BorderRadius.circular(15),
                                         ),
                                         child: Column(
                                           children: [
                                             Icon(Icons.star, color: Colors.amber, size: 30,),
                                             Text('4.2', style: blackStyle,),
                                           ],
                                         ),
                                       ),
                                     ),

                                     Positioned(
                                       bottom: 10,
                                       left: 10,
                                       child: Column(
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           Text("Nha Trang", style: whiteTitleStyle,),
                                         ],
                                       ),
                                     ),

                                   ],
                                 ),
                                 SizedBox(width: 15),
                                 Container(
                                   margin: EdgeInsets.only(bottom: 30),
                                   child: Row(
                                     children: [
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.star, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),

                                     ],
                                   ),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(bottom: 30),
                                   child: Row(
                                     children: [
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.star, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),

                                     ],
                                   ),
                                 ),
                               ],
                             ),
                             Row(
                               children: [
                                 Stack(
                                   children: [

                                     ClipRRect(
                                         borderRadius: BorderRadius.circular(35),
                                         child: Image.asset("img1.jpg",
                                           height: 220,
                                           width: 180,
                                           fit: BoxFit.cover,)
                                     ),
                                     Positioned(
                                       top: 10,
                                       right: 10,
                                       child: Container(
                                         height: 50,
                                         width: 50,
                                         alignment: Alignment.center,
                                         decoration: BoxDecoration(
                                           color: Colors.white54,
                                           borderRadius: BorderRadius.circular(15),
                                         ),
                                         child: Column(
                                           children: [
                                             Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                             Text('4.2', style: blackStyle,),
                                           ],
                                         ),
                                       ),
                                     ),

                                     Positioned(
                                       bottom: 10,
                                       left: 10,
                                       child: Column(
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           Text("Nha Trang", style: whiteTitleStyle,),
                                         ],
                                       ),
                                     ),

                                   ],
                                 ),
                                 SizedBox(width: 15),
                                 Stack(
                                   children: [

                                     ClipRRect(
                                         borderRadius: BorderRadius.circular(35),
                                         child: Image.asset("img1.jpg",
                                           height: 220,
                                           width: 180,
                                           fit: BoxFit.cover,)
                                     ),
                                     Positioned(
                                       top: 10,
                                       right: 10,
                                       child: Container(
                                         height: 50,
                                         width: 50,
                                         alignment: Alignment.center,
                                         decoration: BoxDecoration(
                                           color: Colors.white54,
                                           borderRadius: BorderRadius.circular(15),
                                         ),
                                         child: Column(
                                           children: [
                                             Icon(Icons.star, color: Colors.amber, size: 30,),
                                             Text('4.2', style: blackStyle,),
                                           ],
                                         ),
                                       ),
                                     ),

                                     Positioned(
                                       bottom: 10,
                                       left: 10,
                                       child: Column(
                                         crossAxisAlignment: CrossAxisAlignment.start,
                                         children: [
                                           Text("Nha Trang", style: whiteTitleStyle,),
                                         ],
                                       ),
                                     ),

                                   ],
                                 ),
                                 SizedBox(width: 15),
                                 Container(
                                   margin: EdgeInsets.only(bottom: 30),
                                   child: Row(
                                     children: [
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.star, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),

                                     ],
                                   ),
                                 ),
                                 Container(
                                   margin: EdgeInsets.only(bottom: 30),
                                   child: Row(
                                     children: [
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.favorite, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),
                                       Stack(
                                         children: [

                                           ClipRRect(
                                               borderRadius: BorderRadius.circular(35),
                                               child: Image.asset("img1.jpg",
                                                 height: 220,
                                                 width: 180,
                                                 fit: BoxFit.cover,)
                                           ),
                                           Positioned(
                                             top: 10,
                                             right: 10,
                                             child: Container(
                                               height: 50,
                                               width: 50,
                                               alignment: Alignment.center,
                                               decoration: BoxDecoration(
                                                 color: Colors.white54,
                                                 borderRadius: BorderRadius.circular(15),
                                               ),
                                               child: Column(
                                                 children: [
                                                   Icon(Icons.star, color: Colors.amber, size: 30,),
                                                   Text('4.2', style: blackStyle,),
                                                 ],
                                               ),
                                             ),
                                           ),

                                           Positioned(
                                             bottom: 10,
                                             left: 10,
                                             child: Column(
                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [
                                                 Text("Nha Trang", style: whiteTitleStyle,),
                                               ],
                                             ),
                                           ),

                                         ],
                                       ),
                                       SizedBox(width: 15),

                                     ],
                                   ),
                                 ),
                               ],
                             ),


                           ],
                         ),
                       ),
                     ),
                   ],
                 ),
               ),

             ],
           ),

         ),
       ),
     );
   }
 }

class LoginScreen extends StatefulWidget{
  const  LoginScreen({Key? key}) : super (key: key);
  @override
  _LoginState createState() => _LoginState();
}
class _LoginState extends State<LoginScreen>{
  TextEditingController usernamelogin = new TextEditingController();
  TextEditingController passwordlogin = new TextEditingController();
  Future logins()async{
    var urllogin =Uri.parse("http://10.0.21.52/Flutter_Apptravel/login.php");
    var response = await http.post(urllogin, body: {
      "username" : usernamelogin.text,
      "password" : passwordlogin.text,
    });
    var user = json.decode(response.body);
    if(user == "Success")
    {
      Fluttertoast.showToast(
          msg: "Đăng Nhập Thành công",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0
      );
      // Navigator.push(context, MaterialPageRoute(builder: (context)=>MainScreen()));
    }
    else
    {
      Fluttertoast.showToast(
          msg: "Username và password Không đúng",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
    }
  }

  final _key = GlobalKey<FormState>();

  void login(){
    if(_key.currentState!.validate())
      {

      }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(135,206,250, 0.3),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
              key:_key,
              child: Column(
                children:<Widget> [
                  Image(image:
                  AssetImage("images/userlogo.png",),),
                  Card(
                    child: TextFormField(
                      controller: usernamelogin,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong username":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Username",
                          hintText: "Enter username"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: passwordlogin,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong password":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.phonelink_lock, size: 30,),
                          ),
                          labelText: "Password",
                          hintText: "Enter password"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  // ignore: deprecated_member_use
                  FlatButton(onPressed: (){}, child: Text("Forgot Password", style: TextStyle(color: Colors.amber, fontWeight: FontWeight.bold),
                  ),
                  ),
                  SizedBox(
                    height: 44,
                    // ignore: deprecated_member_use
                    child: RaisedButton(
                      onPressed: (){
                        logins();
                      },
                      color: Colors.lightBlueAccent,
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.black,fontSize: 19),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Have an account?", style: TextStyle(color: Colors.white),),
                      FlatButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>Register()));
                      },
                        child: Text(
                          "Sign Up",
                          style: TextStyle(
                              color: Colors.amber,
                              fontWeight: FontWeight.bold
                          ),
                        ),)
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}


class Register extends StatefulWidget{
  const  Register({Key? key}) : super (key: key);
  @override
  _RegisterState createState() => _RegisterState();
}
class _RegisterState extends State<Register>{
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController passwordconfirm = new TextEditingController();
  TextEditingController fullname = new TextEditingController();
  TextEditingController gender = new TextEditingController();
  TextEditingController roles = new TextEditingController();

  Future registers()async{
    var urlre =Uri.parse("http://10.0.21.52/Flutter_Apptravel/file.php");
    var response = await http.post(urlre, body: {
      "fullname" : fullname.text,
      "username" : username.text,
      "password" : password.text,
      "gioitinh" : gender.text
    });
    var data = json.decode(response.body);
    if(data == "Error")
    {
        Fluttertoast.showToast(
            msg: "Nguoi dung Da Ton tai",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0
        );
    }
    else
    {
        Fluttertoast.showToast(
            msg: "Dang Ky Thanh Cong",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.green,
            textColor: Colors.white,
            fontSize: 16.0
        );
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color.fromRGBO(135,206,250, 0.3),
      body: Center(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Form(
              child: Column(
                children:<Widget> [
                  Image(image: AssetImage("images/add_user.png"),height: 100,),
                  Text('Đăng Ký', style: TextStyle(color: Colors.amber, fontSize: 40),),
                  Card(
                    child: TextFormField(
                      controller: username,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong username":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Username",
                          hintText: "Enter username"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: fullname,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong Ten nguoi dung":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.account_balance, size: 30,),
                          ),
                          labelText: "Fullname",
                          hintText: "Enter fullname"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: password,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong password":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.lock, size: 30,),
                          ),
                          labelText: "Password",
                          hintText: "Enter Password"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: passwordconfirm,
                      validator: (values)=> passwordconfirm.text != passwordconfirm.text?"Khong khop":null,
                      style: TextStyle(fontSize: 20),
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.confirmation_num, size: 30,),
                          ),
                          labelText: "Password Confirm",
                          hintText: "Enter password confirm"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Card(
                    child: TextFormField(
                      controller: gender,
                      validator: (e)=>e!.isEmpty?"Khong duoc de trong Gioi tinh":null,
                      style: TextStyle(fontSize: 20),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(10),
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(left: 20, right:15),
                            child: Icon(Icons.person, size: 30,),
                          ),
                          labelText: "Gender",
                          hintText: "Enter Gender"
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  // ignore: deprecated_member_use

                  SizedBox(
                    height: 44,
                    // ignore: deprecated_member_use
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      // ignore: deprecated_member_use
                      child: RaisedButton(
                        onPressed: (){
                          registers();
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                        },
                        color: Colors.lightBlueAccent,
                        child: Text(
                          "Register",
                          style: TextStyle(color: Colors.black,fontSize: 19),
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Login here ->", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                      // ignore: deprecated_member_use
                      FlatButton(onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginScreen()));
                      },
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: Colors.amber,
                              fontWeight: FontWeight.bold
                          ),
                        ),)
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}


class MyHomePage extends StatefulWidget{
  final name;
  final email;
  final id;
  MyHomePage({this.name = "Guest", this.email = "", this.id});
  @override
  _MyHomePageState createState() => _MyHomePageState();

}
class _MyHomePageState extends State<MyHomePage>{

  var datenow = DateFormat("d MMMM y").format(DateTime.now());
  double _drawerIconSize = 24;
  double _drawerFontSize = 17;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    Widget MenuDrawer(){
      return Drawer(child:  ListView(children:<Widget> [
        UserAccountsDrawerHeader(
          currentAccountPicture: GestureDetector(child:
          CircleAvatar(child: Icon(Icons.person),
            backgroundColor: Colors.amberAccent,
          ),
          ),
          accountName: Text(widget.name),
          accountEmail: Text(widget.email),
        ),
        ListTile(
          onTap: (){},
          leading: Icon(Icons.home, color: Colors.lightGreen,),
          title: Text("Trang chủ", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.lightGreen),),
        ),
        ListTile(
          onTap: (){
            // Navigator.push(context, MaterialPageRoute(builder: (context)=> ProfilePage()));

          },
          leading: Icon(Icons.label, color: Colors.blueAccent,),
          title: Text("Cá Nhân", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blueAccent),),
        ),

        widget.name == "Guest" ?

        ListTile(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginPage(),),);
          },
          leading: Icon(Icons.login, color: Colors.deepPurpleAccent,),
          title: Text("Đăng Nhập", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurpleAccent),),
        ) :
        ListTile(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> MyHomePage(),),);
          },
          leading: Icon(Icons.logout, color: Colors.deepPurpleAccent,),
          title: Text("Đăng Xuất", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurpleAccent),),
        ),
      ],),);
    }


    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 1,
        actions: <Widget>[
          Container(
            width: 150,
            height: 50,
            child: TextField(
              decoration: InputDecoration(
                labelText: 'Tìm kiếm',
                prefixIcon: Icon(Icons.search, color: Colors.grey,),
              ),
            ),
          ),
        ],
      ),
      drawer: MenuDrawer(),
      body: ListView(
        children: <Widget>[
          Padding(padding:  EdgeInsets.all(10) ,child: Text('Discover App', style: TextStyle(fontSize: 30, fontFamily: 'Arial', fontWeight: FontWeight.bold),),
          ),
          // TopPostCard(),
        TopPlaceCard(),
          const SizedBox(height: 15),
         Container(
           margin: EdgeInsets.only(bottom: 10),
           child:  Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: const [
               Text("Cảm hứng du lịch", style: blackStyle,),
               Text("Xem", style: greyStyle,),
             ],
           ),
         ),
          CityListItem(),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset("img2.jpg", width: 90, height: 90, fit: BoxFit.cover,),),
                    Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Đảo Phú Quốc", style: blackStyle),
                        Text("Giá Tham Khảo", style: blackStyle),
                        Row(
                          children: [
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const Text("3.5",style: blackStyle,),
              ],
            ),
          ),

          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset("img2.jpg", width: 90, height: 90, fit: BoxFit.cover,),),
                    Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Đảo Phú Quốc", style: blackStyle),
                        Text("Giá Tham Khảo", style: blackStyle),
                        Row(
                          children: [
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const Text("3.5",style: blackStyle,),
              ],
            ),
          ),
          // SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset("img2.jpg", width: 90, height: 90, fit: BoxFit.cover,),),
                    Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("Đảo Phú Quốc", style: blackStyle),
                        Text("Giá Tham Khảo", style: blackStyle),
                        Row(
                          children: [
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                            Icon(Icons.star, color: Colors.amber,),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                const Text("3.5",style: blackStyle,),
              ],
            ),
          ),
        ],
      ),
    );
  }
}




